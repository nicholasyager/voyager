extern crate simplelog;

#[macro_use]
extern crate log;

use simplelog::*;
use std::fs::File;

use voyager::ship::{Ship, Voyager};
use voyager::strategy::Strategy;
use voyager::universe::{System, SystemType, Universe};
use voyager::Vector2;

fn main() {
    CombinedLogger::init(vec![
        TermLogger::new(LevelFilter::Debug, Config::default()).unwrap(),
        WriteLogger::new(
            LevelFilter::Info,
            Config::default(),
            File::create("my_rust_binary.log").unwrap(),
        ),
    ])
    .unwrap();

    let width = 1000_f64;
    let height = 1000_f64;

    let mut universe = Universe::new(width, height, (width * height * 0.4) as u32, 0.1, 0.5);

    info!(
        "Creating universe of width {:?} and height {:?}",
        width, height
    );

    let default_sensor_range = 10_f64;
    let mut ship = Voyager::new(
        Vector2 {
            x: width - default_sensor_range,
            y: height - default_sensor_range,
        },
        Vector2 { x: 0_f64, y: 0_f64 },
        1_f64,
        default_sensor_range,
    );
    info!("Placing a ship. {:?}", ship);

    info!("Starting simulation.");

    loop {
        let systems = universe.get_systems();
        info!("Simulatimg time {}", universe.get_time());

        // Pick a new destination
        let destination = ship.select_destination(systems);
        debug!("Setting destination to {:?}", destination);

        // Calculate the heading, distance, and time of flight
        let trajectory = destination.subtract(&ship.position);
        debug!("Trajectory: {:?}", trajectory);
        let heading = trajectory.to_unit();
        debug!("Heading: {:?}", heading);
        let distance: f64 = trajectory.magnatude();
        debug!("Distance: {:?}", distance);
        debug!("Ship Speed: {:?}", ship.speed);

        // Set heading.
        ship.set_heading(heading);
        let time_delta: f64;
        if distance / ship.speed > 0.1_f64 {
            time_delta = 0.1_f64;
        } else {
            time_delta = distance / ship.speed;
        }

        // Update universe.
        ship.step(time_delta);
        universe.update_time(time_delta);

        if distance <= 1.0_f64 {
            if destination.x == 0_f64 && destination.y == 0_f64 {
                info!("The arrived home after {} steps!", universe.get_time());
                return;
            }

            info!("The ship has arrived at its destination: {:?}", destination);
            let visited_system: Option<&mut System> = universe.system_visited(destination);
            match visited_system {
                Some(system) => match system.system_type {
                    SystemType::FRIENDLY => {
                        ship.upgrade();
                    }
                    _ => {}
                },
                None => {}
            }
        }
    }
}
