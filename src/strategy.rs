use crate::universe::{SystemType, System};
use crate::Vector2;

pub trait Strategy {
    fn select_destination(&self, systems: &Vec<System>) -> Vector2;
}
