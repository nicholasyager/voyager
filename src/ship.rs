use crate::strategy::Strategy;
use crate::universe::{System, SystemType};
use crate::Vector2;

pub trait Ship {
    fn set_heading(&mut self, heading: Vector2);

    fn step(&mut self, time_of_flight: f64);

    fn upgrade(&mut self);

    fn defend(&mut self);
}

#[derive(Debug)]
pub struct Voyager {
    pub position: Vector2,
    pub heading: Vector2,
    pub speed: f64,
    sensor_range: f64,
}

impl Voyager {
    pub fn new(position: Vector2, heading: Vector2, speed: f64, sensor_range: f64) -> Voyager {
        Voyager {
            position,
            heading,
            speed,
            sensor_range,
        }
    }
}

impl Ship for Voyager {
    fn set_heading(&mut self, heading: Vector2) {
        self.heading = heading
    }

    fn step(&mut self, time_of_flight: f64) {
        if time_of_flight == 0_f64 {
            return;
        }

        self.position = self
            .position
            .add(&self.heading.multiply(self.speed * time_of_flight));
    }

    fn upgrade(&mut self) {
        self.speed += 0.1;
    }

    fn defend(&mut self) {
        self.speed -= 0.1;
    }
}

fn filter_systems(systems: &Vec<System>, filter_system_type: SystemType) -> Vec<&System> {
    systems
        .iter()
        .filter(|system| {
            if system.system_type == filter_system_type {
                true
            } else {
                false
            }
        })
        .collect::<Vec<&System>>()
}

fn distance_from_line(p1: &Vector2, p2: &Vector2, p3: &Vector2) -> f64 {
    ((p2.y - p1.y) * p3.x - (p2.x - p1.x) * p3.y + p2.x * p1.y - p2.y * p1.x).abs()
        / ((p2.y - p1.y).powf(2.0f64) + (p2.x - p1.x).powf(2.0_f64)).powf(0.5_f64)
}

impl Strategy for Voyager {
    fn select_destination(&self, systems: &Vec<System>) -> Vector2 {
        let main_destination = Vector2 { x: 0_f64, y: 0_f64 };

        // Can we get home within a reasonable period of time? If so, let's do it.
        if main_destination.subtract(&self.position).magnatude() / self.speed <= 5.0_f64 {
            return main_destination;
        }

        // Let's ignore places we've already visted.
        let unexplore_systems = systems
            .into_iter()
            .filter(|system| !system.visited)
            .map(|system| (*system).clone())
            .collect::<Vec<System>>();

        // If there's a friendly planet near our path, then let's go to it.
        let friendly_systems: Vec<&System> =
            filter_systems(&unexplore_systems, SystemType::FRIENDLY);

        let mut distances: Vec<(&Vector2, f64, f64, f64, f64)> = friendly_systems
            .iter()
            .map(|system| {
                (
                    &system.position,
                    system.position.subtract(&self.position).magnatude(),
                    self.position.subtract(&main_destination).magnatude(),
                    system.position.subtract(&main_destination).magnatude(),
                    distance_from_line(&self.position, &main_destination, &system.position),
                )
            })
            .filter(
                |(
                    position,
                    ship_to_system,
                    ship_distance,
                    system_distance,
                    distance_from_route,
                )| {
                    if system_distance <= ship_distance {
                        true
                    } else {
                        false
                    }
                },
            )
            .collect::<Vec<(&Vector2, f64, f64, f64, f64)>>();
        distances.sort_by(|a, b| (a.4 + a.1).partial_cmp(&(b.4 + b.1)).unwrap());

        if distances.len() > 0 {
            let system = distances.get(0).unwrap().0.clone();
            info!("Targeting friendly system. {:?}", system);
            return system;
        }

        // If there's a planet on our path that we don't now the type of,
        // then let's go to it.

        // Otherwise, maintain a course for Earth.
        main_destination
    }
}
