#[macro_use]
extern crate log;

pub mod ship;
pub mod strategy;
pub mod universe;

extern crate rand;

#[derive(Debug, Clone, PartialEq)]
pub struct Vector2 {
    pub x: f64,
    pub y: f64,
}

impl Vector2 {
    pub fn add(&self, input: &Vector2) -> Vector2 {
        Vector2 {
            x: self.x + input.x,
            y: self.y + input.y,
        }
    }

    pub fn subtract(&self, input: &Vector2) -> Vector2 {
        Vector2 {
            x: self.x - input.x,
            y: self.y - input.y,
        }
    }

    pub fn magnatude(&self) -> f64 {
        (self.x.powf(2_f64) + self.y.powf(2_f64)).powf(0.5_f64)
    }

    pub fn multiply(&self, input: f64) -> Vector2 {
        Vector2 {
            x: self.x * input,
            y: self.y * input,
        }
    }

    pub fn divide(&self, input: f64) -> Vector2 {
        Vector2 {
            x: self.x / input,
            y: self.y / input,
        }
    }

    pub fn to_unit(&self) -> Vector2 {
        self.divide(self.magnatude())
    }
}
