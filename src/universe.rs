use crate::Vector2;
use rand::Rng;

#[derive(Debug, Clone, PartialEq)]
pub enum SystemType {
    FRIENDLY,
    UNFRIENDLY,
    NEUTRAL,
}

#[derive(Debug)]
pub struct Universe {
    width: f64,
    height: f64,
    systems: Vec<System>,
    time: f64,
}

#[derive(Debug, Clone)]
pub struct System {
    pub position: Vector2,
    pub inhabited: bool,
    pub system_type: SystemType,
    pub visited: bool,
}

impl Universe {
    pub fn new(
        width: f64,
        height: f64,
        system_count: u32,
        populated_worlds: f64,
        friendly_worlds: f64,
    ) -> Universe {
        let mut rng = rand::thread_rng();
        let mut systems = Vec::new();

        for _ in 1..system_count {
            let inhabited = rng.gen_bool(populated_worlds);
            let system_type = match inhabited {
                true => match rng.gen_bool(friendly_worlds) {
                    true => SystemType::FRIENDLY,
                    false => SystemType::UNFRIENDLY,
                },
                false => SystemType::NEUTRAL,
            };

            systems.push(System {
                position: Vector2 {
                    x: rng.gen_range(0_f64, width),
                    y: rng.gen_range(0_f64, height),
                },
                inhabited,
                system_type,
                visited: false,
            });
        }

        Universe {
            width: width,
            height: height,
            systems: systems,
            time: 0_f64,
        }
    }

    pub fn get_time(&self) -> f64 {
        self.time.clone()
    }

    pub fn get_systems(&self) -> &Vec<System> {
        &self.systems
    }

    pub fn update_time(&mut self, delta: f64) {
        self.time += delta;
    }

    pub fn system_visited(&mut self, destination: Vector2) -> Option<&mut System> {
        for system in &mut self.systems {
            if system.position == destination {
                system.visited = true;
                return Some(system);
            }
        }
        error!("Unable to update {:?} to visited!", destination);
        None
    }
}
