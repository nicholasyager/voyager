# Voyager

A simulation of a lone ship traverising a vast galaxy to get home.

## Quick Start

To run a quick simulation, simply execute

```bash
cargo run
```